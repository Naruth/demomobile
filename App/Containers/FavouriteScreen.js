import React, { Component } from 'react';
import { View, Text,TouchableOpacity,FlatList,Image } from 'react-native';
import { Card,Icon } from 'native-base'
import { Colors, Metrics, Images,Fonts } from '../Themes'

import styles from './Styles/FavouriteScreenStyles'

export default class FavouriteScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryList:[1,2,3],
            key_tab: 'places',
            tabMenu: [
                {
                    key: "places",
                    title: "Places"
                },
                {
                    key: "food",
                    title: 'Food',
                }
            ],
        };
        this.type_clicked = "places";
    }
    _handlePressMenuTabView = (tab) => {
        this.type_clicked = tab.key
        this.setState({ tabMenu: [...this.state.tabMenu], key_tab: tab.key });
    }
    renderListView = ({item,index})=>{
        const { key_tab } = this.state
        return(
            <Card style={styles.viewCard}>
                <Image style={{ width:Metrics.screenWidth/3.5,height:Metrics.screenWidth/3.5}} source={key_tab =='food'?Images.berverages:Images.bakery}/>
                <View style={{flex:1,flexDirection:'column',justifyContent:'center',padding:5,paddingLeft:10}}>
                    <Text style={{ fontSize: Fonts.size.regular,color:Colors.text_black}}>Little Creatures Club Street</Text>
                    <View style={{flexDirection:'row',alignItems:'center',paddingTop:5}}>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Icon type="FontAwesome" name="map-marker" style={{fontSize:15,color:Colors.steel}}/>
                            <Text style={{fontSize:Fonts.size.medium,color:Colors.steel,paddingLeft:5}}>856 Esta Underpass</Text>
                        </View>
                    </View>
                    <View style={{justifyContent:'space-between',flexDirection:'row',alignItems:'center',paddingTop:5}}>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Icon type="FontAwesome" name="star" style={{fontSize:15,color:Colors.bloodOrange}}/>
                            <Text style={{fontSize:Fonts.size.medium,color:Colors.text_black,paddingLeft:5}}>4.8</Text>
                            <Text style={{fontSize:Fonts.size.medium,color:Colors.steel,paddingLeft:3}}>(233 ratings)</Text>
                        </View>
                        <View style={{backgroundColor:Colors.bloodOrange,justifyContent:'center',alignItems:'center',borderRadius:20,padding:5,paddingTop:0,paddingBottom:0}}>
                            <Text style={{fontSize:11,color:Colors.snow}}>Free delivery</Text>
                        </View>
                    </View>
                </View>
            </Card>
        )
    }
    render() {
        const { tabMenu,categoryList } = this.state
        return (
            <View style={styles.container}>
                <View style={styles.viewTab}>
                    {tabMenu.map((eachMenu, index) => {
                        IsTab = this.state.key_tab == eachMenu.key ? true : false;
                        return (
                            <View style={{ width: '40%'}}>
                                <TouchableOpacity onPress={() => this._handlePressMenuTabView(eachMenu, index)} key={index} style={{ width: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor:IsTab ? Colors.bloodOrange :null, height:40,borderTopLeftRadius:index==0?20:0,borderBottomLeftRadius:index==0?20:0,borderWidth:0.7,borderColor:IsTab ? Colors.bloodOrange:Colors.steel,borderTopRightRadius:index==1?20:0,borderBottomRightRadius:index==1?20:0}}>
                                    <Text style={{ textAlign: 'center', color: IsTab ? Colors.snow : Colors.steel }}>{eachMenu.title}</Text>
                                </TouchableOpacity>
                            </View>
                        )
                    })}
                </View>
                <View style={styles.subContainer}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={categoryList}
                        renderItem={this.renderListView}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </View>
        );
    }
}
