import React, { Component } from 'react'
import { View, StatusBar,Dimensions,Platform,StyleSheet } from 'react-native'
import { Container } from 'native-base'
import NavigationRouter from '../Navigation/NavigationRouter'
import HeaderScreen from './HeaderScreen';
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import ReduxPersist from '../Config/ReduxPersist'
import { Colors } from '../Themes'

const MyStatusBar = ({ backgroundColor, ...props }) => (
	<View style={[styles.statusBar, { backgroundColor }]}>
	  <StatusBar translucent backgroundColor={backgroundColor} {...props} />
	</View>
);
class RootContainer extends Component {
	componentWillMount = () => {
		StatusBar.setBackgroundColor(Colors.background, true);
	}
	componentDidMount() {
		// if redux persist is not active fire startup action
		if (!ReduxPersist.active) {
			this.props.startup()
		}
		setTimeout(function () {
			StatusBar.setBackgroundColor(Colors.background, true);
		}.bind(this), 2000);
	}

	render() {
		const StatusBarAPP = (<MyStatusBar backgroundColor={Colors.background} barStyle="light-content" />);
		console.disableYellowBox = true;
		return (
			<View style={styles.container}>
				{StatusBarAPP}
				<View style={styles.content}>
					<Container>
						<HeaderScreen/>
						<NavigationRouter />
					</Container>
				</View>
			</View>
		)
	}
}

const { height } = Dimensions.get('window');
const STATUSBAR_HEIGHT = (Platform.OS === 'ios' && height >= 812) ? 44 :(Platform.OS === 'android') && height >=640?17:22;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#fff'
  },
});

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
	startup: () => dispatch(StartupActions.startup())
})

export default connect(null, mapDispatchToProps)(RootContainer)
