import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, Platform, Dimensions } from 'react-native';
import { Icon } from 'native-base';
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import { Colors, Images, ApplicationStyles, Fonts, Metrics } from '../Themes'

const { width, height } = Dimensions.get('window');

class HeaderScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    handlePressBack = () => {
        Actions.pop();
    }

    render() {
        const { data } = this.props.getHeader;
        var title = "";
        var screen = "";
        var statusLogo = true;
        var statusRightIcon = false;  
        if (data) {
            let { titleHeader, statusHeader, screenName } = data
            title = titleHeader;
            screen = screenName;
            // if (screen == "HomeScreen" || screen == "CartScreen" || screen == "MessageScreen" || screen == "MoreScreen") {
            //     statusLogo = true
            // }
            
            if (!statusHeader) {
                return false;
            }
        }
        handleOnRightIcon = () => {
            // alert(Actions.currentScene())
        }
        return (
            <View style={[ApplicationStyles.mainHeaderFooterContainer, { backgroundColor: Colors.snow, paddingTop: Platform.OS == "android" ? 4 : 0 }]}>
                <View style={[ApplicationStyles.mainHeightIconAndTitle, { flexDirection: 'row' }]}>
                    <View style={{ width: '10%' }} />
                    <View style={{ width: '80%', height: 50, justifyContent: 'center' }}>
                        <Text style={{ fontSize: Fonts.size.input, color: Colors.text_black, textAlign: 'center' }}>{title}</Text>
                    </View>
                    <View style={{ width: '10%' }} />
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        getHeader: state.header
    }
}

export default connect(mapStateToProps,null)(HeaderScreen)
