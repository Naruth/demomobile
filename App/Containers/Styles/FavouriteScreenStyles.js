import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
    container:{
        flex:1
    },
    subContainer:{
        flex:1,
        backgroundColor:Colors.main_container,
        padding:Metrics.marginHorizontal
    },
    viewTab:{
        flexDirection: 'row',
        justifyContent:"center",
        marginTop:20,
        marginBottom:20
    },
    viewCard:{
        flexDirection:'row',
        height:Metrics.screenWidth/3.5,
        marginBottom:10,
        borderRadius:5
    }
})
