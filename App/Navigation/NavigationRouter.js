import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { connect } from 'react-redux'
import HeaderActions from '../Redux/HeaderRedux';
import LaunchScreen from '../Containers/LaunchScreen'
import FavouriteScreen from '../Containers/FavouriteScreen'
class NavigationRouter extends Component {
    constructor(props) {
        super(props);
    }
    onEnter = (statusHeader, titleHeader, screenName) => {
        this.props.setHeader({ statusHeader: statusHeader, titleHeader: titleHeader, screenName: screenName })
    }
    render() {
        return (
            <Router>
                <Scene key="root">
                    {/* <Scene onEnter={() => this.onEnter(true, 'My Favourite', 'LaunchScreen')}  key="LaunchScreen" component={LaunchScreen} hideNavBar={true} /> */}
                    <Scene onEnter={() => this.onEnter(true, 'My Favourite', 'FavouriteScreen')} initial={true} key="FavouriteScreen" component={FavouriteScreen} hideNavBar={true} />
                </Scene>
            </Router>
        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setHeader: (data) => dispatch(HeaderActions.setHeaderRequest(data))
    }
}

export default connect(null, mapDispatchToProps)(NavigationRouter)

